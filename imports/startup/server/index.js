import { ServiceConfiguration } from 'meteor/service-configuration';
import { Accounts } from 'meteor/accounts-base';

// This defines a starting set of data to be loaded if the app is loaded with an empty db.
import './fixtures.js';

// This file configures the Accounts package to define the UI of the reset password email.
import './reset-password-email.js';

// Set up some rate limiting and other important security settings.
import './security.js';

// This defines all the collections, publications and methods that the application provides
// as an API to the client.
import './register-api.js';

ServiceConfiguration.configurations.upsert(
  { service: 'facebook' },
  { $set: { appId: '686996481464415', secret: 'dccf74859d5961875bef48825878efb1' } }
);

ServiceConfiguration.configurations.upsert(
  { service: 'google' },
  { $set: { clientId: '948556574030-dgrbs8mpps1v1dl8re2ecper0a8vfcru.apps.googleusercontent.com', secret: 'fVR-7YNDHznWNXvAxdtvSyKz' } }
);

Accounts.onCreateUser( (options, user) => {

  console.log(user);

  let profile = {};

  if(user.services.password) {
    profile.email = user.emails[0].address;
  } else if(user.services.facebook) {
    profile.email = user.services.facebook.email;
    profile.name = user.services.facebook.name;
    profile.picture = "http://graph.facebook.com/" + user.services.facebook.id + "/picture/?type=large";
  } else if(user.services.google) {
    profile.email = user.services.google.email;
    profile.name = user.services.google.name;
    profile.picture = user.services.google.picture;
  }

  user.profile = profile;

  return user;
});
