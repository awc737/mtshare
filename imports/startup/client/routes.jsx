import React from 'react';
import { IndexRedirect, Router, Route, browserHistory } from 'react-router';
import i18n from 'meteor/universe:i18n';

// route components
import AppContainer from '../../ui/containers/AppContainer.jsx';
import GuestContainer from '../../ui/containers/GuestContainer.jsx';
import ListPageContainer from '../../ui/containers/ListPageContainer.jsx';
import TripPageContainer from '../../ui/containers/TripPageContainer.jsx';
import AuthPageSignIn from '../../ui/pages/AuthPageSignIn.jsx';
import AuthPageJoin from '../../ui/pages/AuthPageJoin.jsx';
import NotFoundPage from '../../ui/pages/NotFoundPage.jsx';
import WelcomePage from '../../ui/pages/WelcomePage.jsx';
import DashboardPage from '../../ui/pages/DashboardPage.jsx';
import TripsPage from '../../ui/pages/TripsPage.jsx';

i18n.setLocale('en');

export const renderRoutes = () => (
  <Router history={browserHistory}>
    <Route component={AppContainer}>
      <Route path="/" component={DashboardPage} />
      <Route path="/trips" component={TripsPage} />
      <Route path="trips/:id" component={TripPageContainer} />
      <Route path="lists/:id" component={ListPageContainer} />
    </Route>
    <Route component={GuestContainer}>
      <Route path="welcome" component={WelcomePage} />
      <Route path="signin" component={AuthPageSignIn} />
      <Route path="join" component={AuthPageJoin} />
    </Route>
    <Route path="*" component={NotFoundPage} />
  </Router>
);
