import React from 'react';
import { Link } from 'react-router';
import i18n from 'meteor/universe:i18n';

import Chip from 'material-ui/Chip';
import Avatar from 'material-ui/Avatar';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import BaseComponent from './BaseComponent.jsx';

export default class UserMenu extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = Object.assign(this.state, { open: false });
    this.toggle = this.toggle.bind(this);
  }

  toggle(e) {
    e.stopPropagation();
    this.setState({
      open: !this.state.open,
    });
  }

  renderLoggedIn() {
    const { open } = this.state;
    const { user, logout } = this.props;
    const email = user.profile.email
    const emailLocalPart = email.substring(0, email.indexOf('@'));

    return (
      <div className="user-menu vertical">
        <MuiThemeProvider>
          <Avatar src={user.profile.picture} size={64} />
        </MuiThemeProvider>

        <span className="display-name">{user.profile.name}</span>
        <span className="display-title">Team Leader</span>

        <MuiThemeProvider>
          <Chip className="upgrade-chip">Upgrade</Chip>
        </MuiThemeProvider>

          <a className="btn-logout" onClick={logout}>
            {i18n.__('components.userMenu.logout')}
          </a>
        </div>
    );
  }

  renderLoggedOut() {
    return (
      <div className="user-menu">
        <Link to="/signin" className="btn-secondary">
          {i18n.__('components.userMenu.login')}
        </Link>
        <Link to="/join" className="btn-secondary">
          {i18n.__('components.userMenu.join')}
        </Link>
      </div>
    );
  }

  render() {
    return this.props.user
      ? this.renderLoggedIn()
      : this.renderLoggedOut();
  }
}

UserMenu.propTypes = {
  user: React.PropTypes.object,
  logout: React.PropTypes.func,
};
