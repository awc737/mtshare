import React from 'react';
import {Card, CardActions, CardHeader, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import LinearProgress from 'material-ui/LinearProgress';

const CreateTripCard = () => (
  <Card className="tripcard">
    <div className="flag">
      <h1><span className="icon-plus" /></h1>
      <CardTitle className="trip-title" title="Start a new Trip" />
    </div>

  </Card>
);

export default CreateTripCard;
