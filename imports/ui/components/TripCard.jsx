import React from 'react';
import {Card, CardActions, CardHeader, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import LinearProgress from 'material-ui/LinearProgress';

const TripCard = () => (
  <Card className="tripcard">
    <div className="trip-header">
      <span className="trip-category">Medical Missions</span>
      <span className="trip-date">Nov. 16 - Nov. 25</span>
    </div>
    <div className="flag"><img src="/images/flags/mex.svg" /></div>
    <CardTitle className="trip-title" title="San Miguel de Allende Mexico" subtitle="by Applegate Christian Fellowship" width="100%" />
    <CardText className="trip-text">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
      Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed ...
    </CardText>
    <div className="budget">
      <span className="amount-raised"><b>$12,550</b> USD</span>
      <LinearProgress className="progress-bar" mode="determinate" value={45} />
      <span className="percent-remaining">81%</span>
      <span className="days-left">120 days left</span>
    </div>
  </Card>
);

export default TripCard;
