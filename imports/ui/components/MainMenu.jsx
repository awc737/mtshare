import React from 'react';
import { Link } from 'react-router';
import i18n from 'meteor/universe:i18n';
import BaseComponent from './BaseComponent.jsx';

export default class MainMenu extends BaseComponent {
  constructor(props) {
    super(props);
  }

  renderLoggedIn() {
    const { user, logout } = this.props;

    return (
      <div className="list-todos">
        <Link to="/" className="link-list-new">
          <img className="nav-icon" src="/images/icons/home.svg" />
          <span>{i18n.__('pages.homePage.home')}</span>
        </Link>
        <Link to="/trips" className="link-list-new">
          <img className="nav-icon" src="/images/icons/trips.svg" />
          <span>My Trips</span>
        </Link>
        <Link to="/fundraisers" className="link-list-new">
          <img className="nav-icon" src="/images/icons/fundraisers.svg" />
          <span>Fundraisers</span>
        </Link>
        <Link to="/discover" className="link-list-new">
          <img className="nav-icon" src="/images/icons/discover.svg" />
          <span>Discover</span>
        </Link>
        <Link to="/notifications" className="link-list-new">
          <img className="nav-icon" src="/images/icons/notifications.svg" />
          <span>Notifications</span>
        </Link>
        <Link to="/settings" className="link-list-new">
          <img className="nav-icon" src="/images/icons/settings.svg" />
          <span>{i18n.__('pages.settingsPage.settings')}</span>
        </Link>
      </div>
    );
  }

  renderLoggedOut() {
    return (
    <div className="list-todos">
      <Link to="/welcome" className="link-list-new">
        <i className="fa fa-plane" aria-hidden="true"></i>
        Welcome
      </Link>
      <Link to="/welcome" className="link-list-new">
        <i className="fa fa-compass" aria-hidden="true"></i>
        {i18n.__('pages.homePage.home')}
      </Link>
      <Link to="/trips" className="link-list-new">
        <i className="fa fa-suitcase" aria-hidden="true"></i>
        My Trips
      </Link>
      <Link to="/fundraisers" className="link-list-new">
        <i className="fa fa-money" aria-hidden="true"></i>
        Fundraisers
      </Link>
      <Link to="/discover" className="link-list-new">
        <i className="fa fa-plane" aria-hidden="true"></i>
        Discover Places
      </Link>
    </div>
    );
  }

  render() {
    return this.props.user
      ? this.renderLoggedIn()
      : this.renderLoggedOut();
  }
}

MainMenu.propTypes = {
  user: React.PropTypes.object,
  logout: React.PropTypes.func
};
