import React from 'react';
import i18n from 'meteor/universe:i18n';
import BaseComponent from './BaseComponent.jsx';

class Loading extends BaseComponent {
  render() {
    return (
      <img
        src="https://cdn0.iconfinder.com/data/icons/free-misc-icon-set-2/512/world-256.png"
        className="loading-app"
        alt={i18n.__('components.loading.loading')}
      />
    );
  }
}

export default Loading;
