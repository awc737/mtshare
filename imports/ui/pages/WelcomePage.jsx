import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Link } from 'react-router';
import i18n from 'meteor/universe:i18n';
import BaseComponent from '../components/BaseComponent.jsx';

import AuthPage from './AuthPage.jsx';

export default class WelcomePage extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = Object.assign(this.state, { errors: {} });
    this.onSubmit = this.onSubmit.bind(this);
  }

  onLoginWithFacebook(event) {
    Meteor.loginWithFacebook({}, (err) => {
      if (err) {
        this.setState({
          errors: { none: err.reason },
        });
      }
      this.context.router.push('/');
    });
  }

  onLoginWithGoogle(event) {
    Meteor.loginWithGoogle({}, (err) => {
      if (err) {
        this.setState({
          errors: { none: err.reason },
        });
      }
      this.context.router.push('/');
    });
  }

  onSubmit(event) {
    event.preventDefault();
    const email = this.email.value;
    const password = this.password.value;
    const errors = {};

    if (!email) {
      errors.email = i18n.__('pages.authPageSignIn.emailRequired');
    }
    if (!password) {
      errors.password = i18n.__('pages.authPageSignIn.passwordRequired');
    }

    this.setState({ errors });
    if (Object.keys(errors).length) {
      return;
    }

    Meteor.loginWithPassword(email, password, (err) => {
      if (err) {
        this.setState({
          errors: { none: err.reason },
        });
      }
      this.context.router.push('/');
    });
  }

  render() {
    const { errors } = this.state;
    const errorMessages = Object.keys(errors).map(key => errors[key]);
    const errorClass = key => errors[key] && 'error';

    const content = (
      <div className="wrapper-auth">
        <h1 className="logo">
          <img className="logo-image" src="https://cdn0.iconfinder.com/data/icons/free-misc-icon-set-2/512/world-32.png" />
          {i18n.__('pages.welcomePage.mission')}
          <span className="logo-trip">{i18n.__('pages.welcomePage.trip')}</span>
        </h1>
        <p className="subtitle-auth">
          {i18n.__('pages.authPageSignIn.signInReason')}
        </p>
        <form onSubmit={this.onSubmit}>
          <div className="list-errors">
            {errorMessages.map(msg => (
              <div className="list-item" key={msg}>{msg}</div>
            ))}
          </div>
          <div className={`input-symbol ${errorClass('email')}`}>
            <input
              type="email"
              name="email"
              ref={(c) => { this.email = c; }}
              placeholder={i18n.__('pages.authPageSignIn.yourEmail')}
            />
            <span
              className="icon-email"
              title={i18n.__('pages.authPageSignIn.yourEmail')}
            />
          </div>
          <div className={`input-symbol ${errorClass('password')}`}>
            <input
              type="password"
              name="password"
              ref={(c) => { this.password = c; }}
              placeholder={i18n.__('pages.authPageSignIn.password')}
            />
            <span
              className="icon-lock"
              title={i18n.__('pages.authPageSignIn.password')}
            />
          </div>
          <button type="submit" className="btn-primary">
            {i18n.__('pages.authPageSignIn.signInButton')}
          </button>
        </form>

        <button onClick={this.onLoginWithFacebook} className="btn-facebook">
          <i className="fa fa-facebook"></i>
          {i18n.__('pages.authPageSignIn.signInWithFacebook')}
        </button>

        <button onClick={this.onLoginWithGoogle} className="btn-google">
          <i className="fa fa-google"></i>
          {i18n.__('pages.authPageSignIn.signInWithGoogle')}
        </button>

      </div>
    );

    const link = (
      <Link to="/join" className="link-auth-alt">
        {i18n.__('pages.authPageSignIn.needAccount')}
      </Link>
    );

    return <AuthPage content={content} link={link} />;
  }
}

WelcomePage.contextTypes = {
  router: React.PropTypes.object,
};
