import React from 'react';
import i18n from 'meteor/universe:i18n';
import BaseComponent from '../components/BaseComponent.jsx';
import MobileMenu from '../components/MobileMenu.jsx';
import Message from '../components/Message.jsx';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TripCard from '../components/TripCard';
import CreateTripCard from '../components/CreateTripCard';

class TripsPage extends BaseComponent {
  render() {
    return (
      <div className="page">
        <nav>
          <MobileMenu />
        </nav>
        <div className="content-scrollable">
          <div className="flexcontainer">

            <MuiThemeProvider>
              <TripCard />
            </MuiThemeProvider>

            <MuiThemeProvider>
              <TripCard />
            </MuiThemeProvider>

            <MuiThemeProvider>
              <CreateTripCard />
            </MuiThemeProvider>

          </div>
        </div>
      </div>
    );
  }
}

export default TripsPage;
