import React from 'react';
import i18n from 'meteor/universe:i18n';
import BaseComponent from '../components/BaseComponent.jsx';
import NotFoundPage from '../pages/NotFoundPage.jsx';
import Message from '../components/Message.jsx';

export default class TripPage extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = Object.assign(this.state, { editingTodo: null });
    this.onEditingChange = this.onEditingChange.bind(this);
  }

  onEditingChange(id, editing) {
    this.setState({
      editingTodo: editing ? id : null,
    });
  }

  render() {
    const { trip, tripExists } = this.props;

    if (!tripExists) {
      return <NotFoundPage />;
    }

    return (
      <div className="page lists-show">
        <div className="content-scrollable list-items">
          <h1>{trip.name}</h1>
          <h2>{trip.description}</h2>
        </div>
      </div>
    );
  }
}

TripPage.propTypes = {
  trip: React.PropTypes.object,
  tripExists: React.PropTypes.bool,
};
