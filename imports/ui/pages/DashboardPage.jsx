import React from 'react';
import i18n from 'meteor/universe:i18n';
import BaseComponent from '../components/BaseComponent.jsx';
import MobileMenu from '../components/MobileMenu.jsx';
import Message from '../components/Message.jsx';

class DashboardPage extends BaseComponent {
  render() {
    return (
      <div className="page">
        <nav>
          <MobileMenu />
        </nav>
        <div className="content-scrollable">
          <div className="content-scrollable">
            <Message title={i18n.__('pages.dashboardPage.dashboard')} />
          </div>
        </div>
      </div>
    );
  }
}

export default DashboardPage;
