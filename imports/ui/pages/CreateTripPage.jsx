import React from 'react';
import i18n from 'meteor/universe:i18n';
import BaseComponent from '../components/BaseComponent.jsx';
import MobileMenu from '../components/MobileMenu.jsx';
import Message from '../components/Message.jsx';

import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import { insert } from '../../api/trips/methods.js';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TripCard from '../components/TripCard';
import CreateTripCard from '../components/CreateTripCard';

class CreateTripPage extends BaseComponent {

  constructor(props) {
    super(props);
    this.createNewTrip = this.createNewTrip.bind(this);
    this.state = Object.assign(this.state, { errors: {}, countryValue: 1 });
    this.onSubmit = this.onSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event, index, countryValue) {
    this.setState({countryValue});
  }

  createNewTrip() {
    const { router } = this.context;
    const tripId = insert.call({ locale: i18n.getLocale() }, (err) => {
      if (err) {
        router.push('/');
        /* eslint-disable no-alert */
        alert(i18n.__('components.listList.newListError'));
      }
    });
    router.push(`/trips/${tripId}`);
  }

  onSubmit(event) {
    event.preventDefault();
    const { router } = this.context;
    const name = this.name.value;
    const description = this.description.value;
    const country = this.state.countryValue;
    const errors = {};

    if (!name) {
      errors.name = i18n.__('pages.createTripPage.nameRequired');
    }
    if (!description) {
      errors.description = i18n.__('pages.createTripPage.descriptionRequired');
    }
    if (!country) {
      errors.country = i18n.__('pages.createTripPage.countryRequired');
    }

    this.setState({ errors });
    if (Object.keys(errors).length) {
      return;
    }

    const tripId = insert.call({ locale: i18n.getLocale(), name: name, description: description, country: country }, (err) => {
      if (err) {
        router.push('/');
        /* eslint-disable no-alert */
        alert(i18n.__('components.listList.newListError'));
      }
      router.push(`/trips/${tripId}`);
    });

  }

  render() {
    const { trips } = this.props;
    const { errors } = this.state;
    const errorMessages = Object.keys(errors).map(key => errors[key]);
    const errorClass = key => errors[key] && 'error';

    return (
      <div className="page form">
        <nav>
          <MobileMenu />
        </nav>
        <div className="content-scrollable">
          <div className="wrapper-auth">

            <h1 className="logo">
              Create a Trip
            </h1>

            <p className="subtitle-auth">
              Create a trip...
            </p>

            <form onSubmit={this.onSubmit}>
              <div className="list-errors">
                {errorMessages.map(msg => (
                  <div className="list-item" key={msg}>{msg}</div>
                ))}
              </div>

              <div className={`input-symbol ${errorClass('name')}`}>
                <input
                  type="text"
                  name="name"
                  ref={(c) => { this.name = c; }}
                  placeholder={i18n.__('pages.createTripPage.name')}
                />
                <span
                  className="icon-email"
                  title={i18n.__('pages.createTripPage.name')}
                />
              </div>

              <div className={`input-symbol ${errorClass('description')}`}>
                <input
                  type="text"
                  name="description"
                  ref={(c) => { this.description = c; }}
                  placeholder={i18n.__('pages.createTripPage.description')}
                />
                <span
                  className="icon-lock"
                  title={i18n.__('pages.createTripPage.description')}
                />
              </div>

                <MuiThemeProvider>
                  <SelectField
                  title="country"
                  className="countryselect"
                  floatingLabelFixed={false}
                  floatingLabelText="Country"
                  value={this.state.countryValue}
                  onChange={this.handleChange}
                  ><MenuItem value={null} primaryText="" />
                  <MenuItem value={'mex'} primaryText="Mexico" />
                  <MenuItem value={'usa'} primaryText="United States" />
                  </SelectField>
                </MuiThemeProvider>

              <button type="submit" className="btn-primary">
                {i18n.__('pages.createTripPage.createTrip')}
              </button>
            </form>

          </div>
        </div>
      </div>
    );
  }
}

export default CreateTripPage;

CreateTripPage.propTypes = {
  trips: React.PropTypes.array,
};

CreateTripPage.contextTypes = {
  router: React.PropTypes.object,
};
