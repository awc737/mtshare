import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Trips } from '../../api/trips/trips.js';
import TripsPage from '../pages/TripsPage.jsx';

const TripsPageContainer = createContainer(() => {

  const publicHandle = Meteor.subscribe('trips.public');
  const loading = !publicHandle.ready();
  const trips = Trips.find().fetch();
  const tripsExists = !!trips;

  return {
    trips,
    tripsExists,
    trips: Trips.find({ $or: [
      { userId: { $exists: false } },
      { userId: Meteor.userId() },
    ] }).fetch(),
  };
}, TripsPage);

export default TripsPageContainer;
