import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Factory } from 'meteor/factory';
import i18n from 'meteor/universe:i18n';

class TripsCollection extends Mongo.Collection {
  insert(trip, callback, locale = 'en') {
    const ourTrip = trip;
    if (!ourTrip.name) {
      const defaultName = i18n.__(
        'api.trips.insert.trip',
        null,
        { _locale: locale }
      );
      let nextLetter = 'A';
      ourTrip.name = `${defaultName} ${nextLetter}`;

      while (this.findOne({ name: ourTrip.name })) {
        // not going to be too smart here, can go past Z
        nextLetter = String.fromCharCode(nextLetter.charCodeAt(0) + 1);
        ourTrip.name = `${defaultName} ${nextLetter}`;
      }
    }

    return super.insert(ourTrip, callback);
  }
  remove(selector, callback) {
    return super.remove(selector, callback);
  }
}

export const Trips = new TripsCollection('Trips');

// Deny all client-side updates since we will be using methods to manage this collection
Trips.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

Trips.schema = new SimpleSchema({
  name: { type: String },
  description: { type: String, optional: true },
  country: { type: String },
  userId: { type: String, regEx: SimpleSchema.RegEx.Id, optional: true },
});

Trips.attachSchema(Trips.schema);

// This represents the keys from Trips objects that should be published
// to the client. If we add secret properties to Trip objects, don't trip
// them here to keep them private to the server.
Trips.publicFields = {
  name: 1,
  description: 1,
  country: 1,
  userId: 1,
};

Factory.define('trip', Trips, {});

Trips.helpers({
  // A trip is considered to be private if it has a userId set
  isPrivate() {
    return false;
  },
  isLastPublicTrip() {
    const publicTripCount = Trips.find({ userId: { $exists: false } }).count();
    return !this.isPrivate() && publicTripCount === 1;
  },
  editableBy(userId) {
    if (!this.userId) {
      return true;
    }

    return this.userId === userId;
  },
});
