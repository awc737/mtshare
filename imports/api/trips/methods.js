import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { _ } from 'meteor/underscore';

import { Trips } from './trips.js';

const TRIP_ID_ONLY = new SimpleSchema({
  tripId: { type: String },
}).validator();

export const insert = new ValidatedMethod({
  name: 'trips.insert',
  validate: new SimpleSchema({
    locale: {
      type: String,
    },
    name: {
      type: String,
    },
    description: {
      type: String,
    },
    country: {
      type: String,
    },
  }).validator(),
  run({ locale, name, description, country }) {
    return Trips.insert({name: name, description: description, country: country}, null, locale);
  },
});

export const makePrivate = new ValidatedMethod({
  name: 'trips.makePrivate',
  validate: TRIP_ID_ONLY,
  run({ tripId }) {
    if (!this.userId) {
      throw new Meteor.Error('api.trips.makePrivate.notLoggedIn',
        'Must be logged in to make private trips.');
    }

    const trip = Trips.findOne(tripId);

    if (trip.isLastPublicTrip()) {
      throw new Meteor.Error('api.trips.makePrivate.lastPublicTrip',
        'Cannot make the last public trip private.');
    }

    Trips.update(tripId, {
      $set: { userId: this.userId },
    });
  },
});

export const makePublic = new ValidatedMethod({
  name: 'trips.makePublic',
  validate: TRIP_ID_ONLY,
  run({ tripId }) {
    if (!this.userId) {
      throw new Meteor.Error('api.trips.makePublic.notLoggedIn',
        'Must be logged in.');
    }

    const trip = Trips.findOne(tripId);

    if (!trip.editableBy(this.userId)) {
      throw new Meteor.Error('api.trips.makePublic.accessDenied',
        'You don\'t have permission to edit this trip.');
    }

    // XXX the security check above is not atomic, so in theory a race condition could
    // result in exposing private data
    Trips.update(tripId, {
      $unset: { userId: true },
    });
  },
});

export const updateName = new ValidatedMethod({
  name: 'trips.updateName',
  validate: new SimpleSchema({
    tripId: { type: String },
    newName: { type: String },
  }).validator(),
  run({ tripId, newName }) {
    const trip = Trips.findOne(tripId);

    if (!trip.editableBy(this.userId)) {
      throw new Meteor.Error('api.trips.updateName.accessDenied',
        'You don\'t have permission to edit this trip.');
    }

    // XXX the security check above is not atomic, so in theory a race condition could
    // result in exposing private data

    Trips.update(tripId, {
      $set: { name: newName },
    });
  },
});

export const remove = new ValidatedMethod({
  name: 'trips.remove',
  validate: TRIP_ID_ONLY,
  run({ tripId }) {
    const trip = Trips.findOne(tripId);

    if (!trip.editableBy(this.userId)) {
      throw new Meteor.Error('api.trips.remove.accessDenied',
        'You don\'t have permission to remove this trip.');
    }

    // XXX the security check above is not atomic, so in theory a race condition could
    // result in exposing private data

    if (trip.isLastPublicTrip()) {
      throw new Meteor.Error('api.trips.remove.lastPublicTrip',
        'Cannot delete the last public trip.');
    }

    Trips.remove(tripId);
  },
});

// Get trip of all method names on Trips
const TRIPS_METHODS = _.pluck([
  insert,
  makePublic,
  makePrivate,
  updateName,
  remove,
], 'name');

if (Meteor.isServer) {
  // Only allow 5 trip operations per connection per second
  DDPRateLimiter.addRule({
    name(name) {
      return _.contains(TRIPS_METHODS, name);
    },

    // Rate limit per connection ID
    connectionId() { return true; },
  }, 5, 1000);
}
