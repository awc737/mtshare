/* eslint-env mocha */

import { Factory } from 'meteor/factory';
import { PublicationCollector } from 'meteor/publication-collector';
import { chai, assert } from 'meteor/practicalmeteor:chai';
import { Random } from 'meteor/random';
import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import { DDP } from 'meteor/ddp-client';

import { Trips } from './trips.js';
import { insert, makePublic, makePrivate, updateName, remove } from './methods.js';
import { Todos } from '../todos/todos.js';
import '../../../i18n/en.i18n.json';

if (Meteor.isServer) {
  // eslint-disable-next-line import/no-unresolved
  import './server/publications.js';

  describe('trips', () => {
    describe('mutators', () => {
      it('builds correctly from factory', () => {
        const trip = Factory.create('trip');
        assert.typeOf(trip, 'object');
        assert.match(trip.name, /Trip /);
      });
    });

    describe('publications', () => {
      const userId = Random.id();

      // TODO -- make a `tripWithTodos` factory
      const createTrip = (props = {}) => {
        const trip = Factory.create('trip', props);
        _.times(3, () => {
          Factory.create('todo', { tripId: trip._id });
        });
      };

      before(() => {
        Trips.remove({});
        _.times(3, () => createTrip());
        _.times(2, () => createTrip({ userId }));
        _.times(2, () => createTrip({ userId: Random.id() }));
      });


      describe('trips.public', () => {
        it('sends all public trips', (done) => {
          const collector = new PublicationCollector();
          collector.collect('trips.public', (collections) => {
            chai.assert.equal(collections.Trips.length, 3);
            done();
          });
        });
      });

      describe('trips.private', () => {
        it('sends all owned trips', (done) => {
          const collector = new PublicationCollector({ userId });
          collector.collect('trips.private', (collections) => {
            chai.assert.equal(collections.Trips.length, 2);
            done();
          });
        });
      });
    });

    describe('methods', () => {
      let tripId;
      let todoId;
      let otherTripId;
      let userId;

      beforeEach(() => {
        // Clear
        Trips.remove({});
        Todos.remove({});

        // Create a trip and a todo in that trip
        tripId = Factory.create('trip')._id;
        todoId = Factory.create('todo', { tripId })._id;

        // Create throwaway trip, since the last public trip can't be made private
        otherTripId = Factory.create('trip')._id;

        // Generate a 'user'
        userId = Random.id();
      });

      describe('makePrivate / makePublic', () => {
        function assertTripAndTodoArePrivate() {
          assert.equal(Trips.findOne(tripId).userId, userId);
          assert.isTrue(Trips.findOne(tripId).isPrivate());
          assert.isTrue(Todos.findOne(todoId).editableBy(userId));
          assert.isFalse(Todos.findOne(todoId).editableBy(Random.id()));
        }

        it('makes a trip private and updates the todos', () => {
          // Check initial state is public
          assert.isFalse(Trips.findOne(tripId).isPrivate());

          // Set up method arguments and context
          const methodInvocation = { userId };
          const args = { tripId };

          // Making the trip private adds userId to the todo
          makePrivate._execute(methodInvocation, args);
          assertTripAndTodoArePrivate();

          // Making the trip public removes it
          makePublic._execute(methodInvocation, args);
          assert.isUndefined(Todos.findOne(todoId).userId);
          assert.isTrue(Todos.findOne(todoId).editableBy(userId));
        });

        it('only works if you are logged in', () => {
          // Set up method arguments and context
          const methodInvocation = { };
          const args = { tripId };

          assert.throws(() => {
            makePrivate._execute(methodInvocation, args);
          }, Meteor.Error, /trips.makePrivate.notLoggedIn/);

          assert.throws(() => {
            makePublic._execute(methodInvocation, args);
          }, Meteor.Error, /trips.makePublic.notLoggedIn/);
        });

        it('only works if it\'s not the last public trip', () => {
          // Remove other trip, now we're the last public trip
          Trips.remove(otherTripId);

          // Set up method arguments and context
          const methodInvocation = { userId };
          const args = { tripId };

          assert.throws(() => {
            makePrivate._execute(methodInvocation, args);
          }, Meteor.Error, /trips.makePrivate.lastPublicTrip/);
        });

        it('only makes the trip public if you made it private', () => {
          // Set up method arguments and context
          const methodInvocation = { userId };
          const args = { tripId };

          makePrivate._execute(methodInvocation, args);

          const otherUserMethodInvocation = { userId: Random.id() };

          // Shouldn't do anything
          assert.throws(() => {
            makePublic._execute(otherUserMethodInvocation, args);
          }, Meteor.Error, /trips.makePublic.accessDenied/);

          // Make sure things are still private
          assertTripAndTodoArePrivate();
        });
      });

      describe('updateName', () => {
        it('changes the name, but not if you don\'t have permission', () => {
          updateName._execute({}, {
            tripId,
            newName: 'new name',
          });

          assert.equal(Trips.findOne(tripId).name, 'new name');

          // Make the trip private
          makePrivate._execute({ userId }, { tripId });

          // Works if the owner changes the name
          updateName._execute({ userId }, {
            tripId,
            newName: 'new name 2',
          });

          assert.equal(Trips.findOne(tripId).name, 'new name 2');

          // Throws if another user, or logged out user, tries to change the name
          assert.throws(() => {
            updateName._execute({ userId: Random.id() }, {
              tripId,
              newName: 'new name 3',
            });
          }, Meteor.Error, /trips.updateName.accessDenied/);

          assert.throws(() => {
            updateName._execute({}, {
              tripId,
              newName: 'new name 3',
            });
          }, Meteor.Error, /trips.updateName.accessDenied/);

          // Confirm name didn't change
          assert.equal(Trips.findOne(tripId).name, 'new name 2');
        });
      });

      describe('remove', () => {
        it('does not delete the last public trip', () => {
          const methodInvocation = { userId };

          // Works fine
          remove._execute(methodInvocation, { tripId: otherTripId });

          // Should throw because it is the last public trip
          assert.throws(() => {
            remove._execute(methodInvocation, { tripId });
          }, Meteor.Error, /trips.remove.lastPublicTrip/);
        });

        it('does not delete a private trip you don\'t own', () => {
          // Make the trip private
          makePrivate._execute({ userId }, { tripId });

          // Throws if another user, or logged out user, tries to delete the trip
          assert.throws(() => {
            remove._execute({ userId: Random.id() }, { tripId });
          }, Meteor.Error, /trips.remove.accessDenied/);

          assert.throws(() => {
            remove._execute({}, { tripId });
          }, Meteor.Error, /trips.remove.accessDenied/);
        });
      });

      describe('rate limiting', () => {
        it('does not allow more than 5 operations rapidly', () => {
          const connection = DDP.connect(Meteor.absoluteUrl());

          _.times(5, () => {
            connection.call(insert.name, { locale: 'en' });
          });

          assert.throws(() => {
            connection.call(insert.name, {});
          }, Meteor.Error, /too-many-requests/);

          connection.disconnect();
        });
      });
    });
  });
}
