/* eslint-disable prefer-arrow-callback */

import { Meteor } from 'meteor/meteor';

import { Trips } from '../trips.js';

Meteor.publish('trips.public', function tripsPublic() {
  return Trips.find({
    userId: { $exists: false },
  }, {
    fields: Trips.publicFields,
  });
});

Meteor.publish('trips.private', function tripsPrivate() {
  if (!this.userId) {
    return this.ready();
  }

  return Trips.find({
    userId: this.userId,
  }, {
    fields: Trips.publicFields,
  });
});
